# Sound Droid #

Example Sound Cloud client for Android. 

### Topics addressed: ###

* Using Retrofit to access REST API's
* Using JSON objects
* Getting images with Picasso
* Media Player
* RecyclerView