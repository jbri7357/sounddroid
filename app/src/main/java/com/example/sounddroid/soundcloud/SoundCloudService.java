package com.example.sounddroid.soundcloud;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by jrbrinkley on 5/15/15.
 */
public interface SoundCloudService {

    static final String CLIENT_ID = "e060d3aee1fc8aaa1375493705618750";

    @GET("/tracks?client_id=" + CLIENT_ID)
    public void searchSongs(@Query("q") String query, Callback<List<Track>> cb);

    @GET("/tracks?client_id=" + CLIENT_ID)
    public void getRecentSongs(@Query("create_at[from]") String date, Callback<List<Track>> cb);


}
